# Howto setup GitLab Docker server on Windows 10 WSL2

## Create a "c:\Docker-GitLab" and "c:\Docker-GitLab\gitlab-backup" folder.

## Run your docker-ce image with this command (PowerShell)
Create volume (do it once)
```
docker volume create gitlab-logs
docker volume create gitlab-data
```
docker run
```
docker run --detach \
	--name gitlab \
	--restart always \
	--hostname pcpc9x.ddns.net \
	--publish 443:443 --publish 80:80 --publish 22:22 \
	--volume C:\Docker-GitLab:/etc/gitlab \
	--volume gitlab-logs:/var/log/gitlab \
	--volume gitlab-data:/var/opt/gitlab \
	--memory="2g"
	--shm-size 256m
	gitlab/gitlab-ce
```

```
docker run --detach --name gitlab --restart always --memory="2g" --hostname pcpc9x.ddns.net --publish 192.168.1.10:443:443 --publish 192.168.1.10:80:80 --publish 192.168.1.10:22:22 --volume C:\Docker-GitLab:/etc/gitlab --volume gitlab-logs:/var/log/gitlab --volume gitlab-data:/var/opt/gitlab --shm-size 256m gitlab/gitlab-ce
```

modem NAT port: 8082:80, 4443:443, 2282:22

## In docker terminal(in docker gui application press to "cli" buton) go here :

```
cd etc/gitlab
vi gitlab.rb
```

[Howto use vi editor](https://www.redhat.com/sysadmin/introduction-vi-editor)

## Go to end of file at gitlab.rb and write these lines :
type G (goto end file)
type i (insert mode)
add:
```
external_url "http://pcpc9x.ddns.net"
```

Modify that to reflect either your server domain or IP address. 
If you're using an IP address, make sure to drop the **https** in favor of **http**

For setup ssh port (do not need in this setup)
```
gitlab_rails['gitlab_shell_ssh_port'] = 2282
```

If you configure GitLab to use a domain, you'll have to enable SSL. To do that, locate the following two lines (around line 1519):
```
letsencrypt['enable'] = true
letsencrypt['contact_emails'] = 'pcpc9x@gmail.com'
```

For backup folder:
```
gitlab_rails['manage_backup_path'] = false
gitlab_rails['backup_path'] = '/etc/gitlab/gitlab-backup'
```
Whenever GitLab is backed up (docker exec -it gitlab gitlab-rake gitlab:backup:create), the backup file(s) get exposed to the host

Save and close to gitlab.rb:
type Esc (command mode)
type ZZ
Only quit (not save): type :q!

## Enter this code for reconfiguration:
```
gitlab-ctl reconfigure
```

## Remove your docker container and run with 2-step command again

## Change root password
CLI in gitlab docker
```
gitlab-rake 'gitlab:password:reset'
```
## Setup NAT in modem
modem NAT port: 8082:80, 4443:443, 2282:22
modem domain: pcpc9x.ddns.net
modem WAN IP: x.x.x.x
modem local IP: 192.168.1.1
PC server local IP: 192.168.1.10
mean:
pcpc9x.ddns.net:8082 -> 192.168.1.10:80 (http)
pcpc9x.ddns.net:4443 -> 192.168.1.10:443 (https)
pcpc9x.ddns.net:2282 -> 192.168.1.10:22 (ssl)

## Setup ssh
https://docs.gitlab.com/ee/user/ssh.html

Edit file ~/.ssh/config like this
```
Host  pcpc9x.ddns.net
	User git
	Hostname  pcpc9x.ddns.net
	Port 2282
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/id_rsa_gitlab_self
```

## Work with git project
Create new project in self-host gitlab server with link like:
http://pcpc9x.ddns.net:howto/git-help

* Work with http
- Clone:
Into your project directory:
```
git clone http://pcpc9x.ddns.net:howto/git-help
cd git-help
```
- Check origin remote
```
git remote -v
origin http://pcpc9x.ddns.net:howto/git-help (fetch)
origin http://pcpc9x.ddns.net:howto/git-help (push)
```
- Edit your file, then update to server
```
git add .
git commit -m "update"
git push origin main
```
When popup show, you must type your user, password of gitlab account to connect

* Work with ssh
- Clone:
Into your project directory:
```
git clone git@pcpc9x.ddns.net:howto/git-help
cd git-help
```
- Check origin remote
```
git remote -v
origin git@pcpc9x.ddns.net:howto/git-help (fetch)
origin git@pcpc9x.ddns.net:howto/git-help (push)
```
- Edit your file, then update to server
```
git add .
git commit -m "update"
git push origin main
```

